#ifndef HANDLE_DIR_H
#define HANDLE_DIR_H
#include <dirent.h>
#include "types.h"

typedef struct{
    char **arr;
    int size;
}tNameArr;

/*
    muestra en salida est�ndar los nombres
    de los archivos que est�n en path
    retorna si se pudo realizar
*/
int listdir(char *path);
/*
    obtiene arreglo de nombres de todos los archivos
    retorna puntero a estructura de arreglo
    o NULL si no se pudo armar.
*/
tNameArr *filenamesdir(char *path);
/*
    obtiene arreglo de nombres de archivos
    de una extensi�n en particular (bmp, jpg, etc)
    retorna puntero a estructura de arreglo
    o NULL si no se pudo armar.
*/
tNameArr *filenamesdirext(char *path, char *ext);
/*
    muestra el listado de archivos guardado
*/
int showfilenames(tNameArr *fileList);
#endif
