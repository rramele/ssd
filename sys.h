#ifndef SYS_H_INCLUDED
#define SYS_H_INCLUDED
//#include "ss.h"
#include "types.h"
#define MOD 251
/* cada grupo tiene k pixeles de 1 byte cada uno*/
typedef struct{
    int     k;
    BYTE    *pixels;
}tGroup;
/* cada imagen tiene r grupos de k pixeles cada uno*/
typedef struct{
    DWORD     r;
    tGroup    *groups;
}tGroupArr;
/*FUNCIONES*/
/*crear sistema vacio de f filas por c columnas + 1*/
tGroupArr createSystem(tGroupArr eqSystem, int f, int c);
/*cargar sistema*/
void loadSystem(tGroupArr *eqSystem);
/*mostrar sistema*/
void showSystem(tGroupArr eqSystem);
/* Resuelve el sistema*/
tGroup solveSystem(tGroupArr sysEq);
/* Muestra el sistema*/
void showSolution(tGroup sol);
#endif // SYSEQ_H_INCLUDED
