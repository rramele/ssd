#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "handleDir.h"
#include "bmpADT.h"
#include "bmpArray.h"
#include "ss.h"

typedef struct{
    BYTE    bfType[2];  /*must be BM*/
    DWORD   bfSize;     /*size of the bitmap file*/
    WORD    bfReserved1;
    WORD    bfReserved2;
    DWORD   bfOffBits;  /*offset in bytes from the beginning of the bitmap file header to the bitmap bits*/
}tbmpFileHeader;

/*
    Uso: ./ssid archivosecreto [directorio]
*/

void getParameter(char *par, int *val, int min, int max);

int
main(int argc, char *argv[])
{
    tNameArr *picList;
    bmpADT  bmpSecret;
    tbmpArray   bmpArray;
    DWORD   imageSize;
    int maxn, n, k;

    if (argc == 1 || argc > 3)
    {
        printf("Ingreso %d argumentos\n", argc);
        printf("Uso: ./sssid archivosecreto [subdir]");
        return EXIT_FAILURE;
    }

    if ((bmpSecret = loadBMP(argv[1]))==NULL)
    {
        printf("El archivo del secreto  no existe!\n");
        return EXIT_FAILURE;
    }
    if(argc == 2) /*buscara imagenes en el directorio actual*/
    {
        picList = filenamesdirext(".", "bmp");
    }else
        {
            picList = filenamesdirext(argv[2],"bmp");
        }
    showBMPInfo(bmpSecret);
    if (picList == NULL)
    {
        printf("No hay imagenes disponibles...\n");
        return EXIT_FAILURE;
    }
    maxn = showfilenames(picList);
    printf("Hay %d imagenes.\n", maxn);
    imageSize = getImageSize(bmpSecret);
    maxn = obtainBmpsSize(picList, imageSize);
    if (maxn == 0)
    {
        printf("No hay imagenes del tamaño de la del secreto.\n");
        return EXIT_FAILURE;
    }
    printf("Hay %d imagenes del tamaño de la del secreto.\n", maxn);
    n=8;//getParameter("n", &n, 3, maxn);
    k=8;//getParameter("k", &k, 2, n);
    bmpArray.size = n;
    if (obtainBmpsSizeArray(picList, imageSize, &bmpArray)==0)
    {
        printf("No se pudo armar el arreglo de imagenes.\n");
        return EXIT_FAILURE;
    }
    normalizeBMP(bmpSecret, MOD-1);
    /* mezclar los pixeles de la imagen*/
    scrambleBMP(bmpSecret);

    printf ("Ready to distribuite everything...\n");
    if (ssDistribute(bmpSecret, &bmpArray, n, k))
    {
        if (!saveAllBmpArray(&bmpArray))
        {
            printf("Error al guardar los archivos  modificados.\n");
        }
    }else printf("Error al distribuir.\n");
    
    /**
    saveBMP(bmpSecret,"secretocambiado.bmp");
    bmpSecret = loadBMP("secretocambiado.bmp");
    descrambleBMP(bmpSecret);
    saveBMP(bmpSecret,"secretoalreves.bmp");
    **/
    freeBmpsArray(&bmpArray);
    return EXIT_SUCCESS;
}
void
getParameter(char *par, int *val, int min, int max)
{
    do
    {
        printf("Ingrese %s (entre %d y %d):", par, min, max);
        scanf("%d", val);
    }while(*val > max || *val < min);
}
