#include <stdlib.h>
#include <time.h>
#include "random.h"
float
rand_normalize(){
	/* Generates between [0, 1) */
	return rand()/(RAND_MAX + 1.0);
}

int
rand_int(int n1, int n2)
{
	return (n2 - n1 + 1) * rand_normalize() + n1;
}

long int
randInt2 (long int maxn)
{
    return (long int) ((maxn + 1) * rand_normalize());
}
unsigned long int *
generaSec(unsigned long int n)
{
long int i, j;
unsigned long int *sec;
sec = malloc((n-1)*sizeof(unsigned long int));
if (sec!=NULL)
{
    for (i = n-1; i >  0; i--)
    {
        j = randInt2(i);
        sec[n-1-i] = j;
    }
}
return sec;
}
void
rand_initialize()
{
	srand(time(NULL));
	/* llamar una vez rand_int porque el primer llamado nunca lo hace random */
	rand_normalize();
}

void
randomizeSeed(void)
{
    srand(SEED);
}
