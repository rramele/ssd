#ifndef SS_H_INCLUDED
#define SS_H_INCLUDED
#include "bmpArray.h"
#include "bmpADT.h"
#define MAXK    9 /*maximo número de sombras exigidas para rearmar el secreto*/
#define MOD     251
/* cada grupo tiene k pixeles de 1 byte cada uno*/
typedef struct{
    int     k;
    BYTE    *pixels;
}tGroup;
/* cada imagen tiene r grupos de k pixeles cada uno*/
typedef struct{
    DWORD     r;
    tGroup    *groups;
}tGroupArr;
typedef struct{
    int n;
    tGroupArr   *shares;
}tShareArr;
/* Distribuye el secreto en n sombras según esquema (n, k)*/
int ssDistribute(bmpADT bmpSecret, tbmpArray *bmpArray, int n, int k);
/* Libera un grupo*/
void freeGroup(tGroup *group);
/* Libera todos los puntos de un arreglo - lo usa sysEq*/
void freeGroupArray(tGroupArr *secret);
#endif // SS_H_INCLUDED
