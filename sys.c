#include <stdio.h>
#include <stdlib.h>
#include "sys.h"
/*static functions*/
static void generateInv(unsigned char inv[], unsigned char module);
static unsigned char inverse(unsigned char number, unsigned char module);
static int triangular(tGroupArr *sys, unsigned char inv[]);
/*desarrollo de las funciones*/
static unsigned char
inverse(unsigned char number, unsigned char module)
{
    unsigned char i;
    for(i = 1; (i*number) % module != 1; i++)
        ;
    return i;
}

static void
generateInv(unsigned char inv[], unsigned char module)
{
    int i;
    for(i = 0; i < module-1; i++)
    {
        inv[i] = inverse(i+1, module);
    }
}
tGroupArr
createSystem(tGroupArr eqSystem, int f, int c)
{
    int i;
    int rta = OK;
    eqSystem.r = f;
    eqSystem.groups = malloc(sizeof (tGroup)*f);
    if (eqSystem.groups != NULL)
    {
        for (i = 0; i < f && rta == OK; i ++)
        {
            eqSystem.groups[i].k = c + 1;
            eqSystem.groups[i].pixels = malloc(c + 1);
            if (eqSystem.groups[i].pixels == NULL)
                rta = FAILURE;
        }
    }
    return eqSystem;
}
void
loadSystem(tGroupArr *eqSystem)
{
    int i;
    int j;
    for (i = 0; i < eqSystem->r; i++)
    {
        for(j = 0; j < eqSystem->groups[i].k; j++)
        {
            printf("Ingrese valor para posici�n %d %d", i, j);
            scanf("%d", &(eqSystem->groups[i].pixels[j]));
        }
    }
}
void
showSystem(tGroupArr eqSystem)
{
    int i;
    int j;
     for (i = 0; i < eqSystem.r; i++)
    {
        printf("\n");
        for(j = 0; j < eqSystem.groups[i].k; j++)
        {
            printf("%d\t",eqSystem.groups[i].pixels[j]);
        }
    }
}
tGroup
solveSystem(tGroupArr sysEq)
{
    tGroup sol;
    unsigned char inv[MOD-1];
    int rta;
    sol.k = sysEq.groups[0].k;
    sol.pixels = calloc(sol.k,1);
    /*obtener matriz inversos*/
    generateInv(inv, MOD);
    /*triangular matriz*/
    rta = triangular(&sysEq, inv);
    /*resolver matriz triangular*/
    return sol;
}
/* Muestra el sistema*/
void
showSolution(tGroup sol)
{
    int i;
    for (i = 0; i < sol.k; i++)
        printf("%d\t", sol.pixels[i]);
}
static int
triangular(tGroupArr *sys, unsigned char inv[])
{
    int pivote;
    int i;
    int f, c;
    f = sys->r;
    c = sys->groups[0].k;
    for (pivote = 0; pivote < f-1; pivote++)
   {
       for (i > pivote; i < f; i++)
       {
           /*restar fila i - fila pivote*/
           /*multiplicar fila i * inv col (i+1)*/
       }
   }
   return OK;
}
