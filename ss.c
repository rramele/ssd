#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "types.h"
#include "ss.h"

/*
    Divide el total de pixels de bmp por k
    para obtener cuántos grupos - bloques de k pixeles hay
*/
static DWORD getNumberOfGroups(bmpADT bmp, int k);
/*
    Guarda en un arreglo los r grupos de k pixeles
    que se obtienen del bmp.
    Retorna OK o FAILURE
*/
static int partitionOneImage(bmpADT bmpSecret, tGroupArr *secret, DWORD r, int k);
/*
    Guarda en un arreglo los r grupos de k pixeles
    de cada una de los n covers
    Retorna OK o FAILURE
*/
static int partitionNImages(tbmpArray *bmpArray, tShareArr *shareArr, DWORD r, int k, int n);
/*
    Carga un conjunto de pixels en el campo pixels del grupo Group
*/
static int load(tGroup *group, BYTE *pixels, int k);
/*
    Calcula las n sombras correspondientes a cada grupo de k pixeles
 */
static int getShadows(tGroupArr secret, int n, tGroupArr *shadows);
/*
    Calcula una sombra segun el indice
*/
static BYTE calculateShadow(tGroup g, DWORD index);
static void freeShareArray(tShareArr *shareArr);
/* save functions*/
static int saveShareAllBmp(tbmpArray *bmpArray, tShareArr *shareArr);
static int saveShareOneBmp(bmpADT bmp, tGroupArr *share);
static char * obtainNewName(char *oldnamewithext,char *add);
/* embed functions*/
static int embedSecret(tShareArr *shareArr, tGroupArr *secret);
static int embedGroupAllImages(tShareArr *shareArr, tGroup groupVal, DWORD pos);
static int embedInImage(tGroup *group, BYTE val);
/* ************************************************************************** */
/* Definitions of functions*/
int
ssDistribute(bmpADT bmpSecret, tbmpArray *bmpArray, int n, int k)
{
    DWORD r;
    tGroupArr   secret;
    tShareArr   shareArr;
    tGroupArr   shadows;
    int rta = OK;
    r = getNumberOfGroups(bmpSecret, k);

    /* 1. partir bmpSecret en r grupos de k pixeles*/

    if (!partitionOneImage(bmpSecret, &secret, r, k))
        return FAILURE;

    /* 2. partir cada una de las n imagenes covers, en r grupos de k pixeles*/

    if (!partitionNImages(bmpArray, &shareArr, r, k, n))
        return FAILURE;

    /* 3. obtener los n shadows correspondientes a c/u de los r grupos*/

    if (!getShadows(secret, n, &shadows))
        return FAILURE;

    /* 4. ocultar los shadows en las imagenes covers*/

    if (!embedSecret(&shareArr, &shadows))
    {
        rta = FAILURE;
    }
    //else
    /* 4. guardar cada share en bmpArray */
     /**if (!saveShareAllBmp(bmpArray, &shareArr))
        {
            rta = FAILURE;
        }
    **/
    /* 5. liberar todo*/

    freeShareArray(&shareArr);
    freeGroupArray(&secret);

    return rta;
}

static DWORD
getNumberOfGroups(bmpADT bmp, int k)
{
    DWORD imSize = getImageSize(bmp);
    return (imSize/k);
}
static BYTE
calculateShadow(tGroup g, DWORD index)
{
    BYTE val;
    int i, k;
    k = g.k;
    val = g.pixels[k-1];
    for (i = k-2; i>=0; i--)
    {
        val = (g.pixels[i]+val*index)%MOD;
    }
    printf("\n");
    return val;
}
static int
getShadows(tGroupArr secret, int n , tGroupArr *shadows)
{
    DWORD i, g;
    int rta = OK;
    shadows->r = secret.r;
    if ((shadows->groups = malloc(sizeof(tGroup)*shadows->r))==NULL)
        return FAILURE;
    for (g = 0; g < secret.r && rta == OK; g++)
    {
        shadows->groups[g].k = n;
        if((shadows->groups[g].pixels = malloc(sizeof(BYTE)*n))==NULL)
            rta = FAILURE;
        for (i = 0; i < n && rta == OK; i++)
        {
           if(g == 0){
                shadows->groups[g].pixels[i] = calculateShadow(secret.groups[g], i+1);

            printf("%ud\n", shadows->groups[g].pixels[i]);
           }
        }
    }
    return rta;
}
static int
partitionNImages(tbmpArray *bmpArray, tShareArr *shareArr, DWORD r, int k, int n)
{
    int i;
    int rta = OK;
    shareArr->n =  n;
    shareArr->shares = malloc(sizeof(tGroupArr)*n);
    if(shareArr->shares == NULL)
        return FAILURE;
    for(i = 0; rta == OK && i < n; i++)
    {
        printf("\nProceso %d,%s", i, getFileName(bmpArray->pics[i]));
        rta = partitionOneImage(bmpArray->pics[i], &(shareArr->shares[i]),r,k);
    }
    return rta;
}
static int
partitionOneImage(bmpADT bmpSecret, tGroupArr *secret, DWORD r, int k)
{
    DWORD i;
    int rta = OK;
    DWORD   block;
    BYTE*   pixels;
    DWORD   imSize;

    secret->r = r;
    if ((secret->groups = malloc(sizeof(tGroup)*secret->r))==NULL)
        return FAILURE;
    i = 0;
    block = 0;
    pixels = NULL;
    imSize = getPixelBlock(bmpSecret, &pixels);
    if (imSize != (r*k))
        return !OK;
    while (rta == OK && i < (r*k))
    {
        /* guardo un bloque */
        load(&secret->groups[block],pixels + i, k);
        block++;
        i += k;
    }
    free(pixels);
    return (rta && (block == r));
}
static int
load(tGroup *group, BYTE *pixels, int k)
{
    group->k = k;
    group->pixels = malloc(k);
    if (group->pixels == NULL)
        return 0;
    memcpy(group->pixels, pixels, k);
    return k;
}
static void
freeShareArray(tShareArr *shareArr)
{
    int i;
    for(i = 0; i < shareArr->n;i++)
    {
        freeGroupArray(&shareArr->shares[i]);
    }
    free(shareArr->shares);
}
void
freeGroupArray(tGroupArr *secret)
{
    DWORD   i;
    for (i = 0; i < secret->r; i++)
    {
        freeGroup(&secret->groups[i]);
    }
    free(secret->groups);
}
void
freeGroup(tGroup *group)
{
    free(group->pixels);
}
static int
saveShareOneBmp(bmpADT bmp, tGroupArr *share)
{
    char *oldname;
    char *newname;
    BYTE    *pixels;
    tGroup  group;
    DWORD   imSize;
    int     i;

    imSize = getImageSize(bmp);
    pixels = malloc(imSize);
    for (i = 0; i < share->r; i++)
    {
        /* concateno cada bloque en pixels */
        group = share->groups[i];
        memcpy(pixels + i * group.k, group.pixels, group.k);
    }
    /*copio todos los pixels en bmp->pixels*/
    loadPixels(bmp, pixels, group.k * share->r);
    /*cambio nombre del archivo*/
    oldname = getFileName(bmp);

    newname = obtainNewName(oldname, "ssd");
    if(newname !=NULL)
    {
        changeFileName(bmp, newname);
    }
    free(pixels);
    return OK;
}
static char *
obtainNewName(char *oldnamewithext,char *add)
{
    char *newname;
    int len;
    int i;
    char *ext;
    i = 0;
    while(oldnamewithext[i]!='\0')
    {
        if (oldnamewithext[i]=='.')
            len = i;
        i++;
    }
    if((ext = malloc(strlen(oldnamewithext)-len))==NULL)
        return NULL;
    strcpy(ext, oldnamewithext + len);
    if((newname = malloc(strlen(oldnamewithext) + strlen(add)+1))==NULL)
        return NULL;
    strcpy(newname, oldnamewithext);
    newname[len] = '\0';
    strcat(newname, add);
    strcat(newname, ext);
    return newname;
}
static int
embedSecret(tShareArr *shareArr, tGroupArr *secret)
{
    int rta = OK;
    DWORD g;
    for (g = 0; g < secret->r; g++)
    { if (g == 0)
        if(!embedGroupAllImages(shareArr, secret->groups[g], g))
        {
            printf("Error al ocultar %d\n", g);
            rta = FAILURE;
        }
    }
    return rta;
}
static int
embedGroupAllImages(tShareArr *shareArr, tGroup groupVal, DWORD g)
{
    int i, n;
    int rta = OK;
    n = shareArr->n;
    for(i = 0; i < n && rta ==OK; i++)
    {
        if(!embedInImage(&(shareArr->shares[i].groups[g]),groupVal.pixels[i]))
        {
            rta = FAILURE;
        }
    }
    return rta;
}
static int
embedInImage(tGroup *group, BYTE val)
{
    BYTE i;
    BYTE bit;
    bit = (val &0x80)>>7;

    for(i = 0; i < 8; i++)
    {
        printf("%d ", val);
        group->pixels[i]=(group->pixels[i]&0xFE)|bit;
        val = val <<1;
        bit = (val &0x80)>>7;
    }
    return OK;
}
