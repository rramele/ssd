CC = gcc
CFLAGS = -lm -lssl -lcrypto
PROG = bin/ssd2015

SRCS = *.c

all: $(PROG)

$(PROG):	$(SRCS)
	$(CC) $(CFLAGS) -o $(PROG) $(SRCS) $(LIBS)

clean:
	cp /Users/rramele/Desktop/images/group/*.bmp /Users/rramele/Desktop/images/group1/
	rm -f $(PROG)
