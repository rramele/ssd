#ifndef BITMAP_ADTH
#define BITMAP_ADTH
#include <stdio.h>
#include <stdint.h>
#include "types.h"


/*  Interface del ADT BMP  */
typedef struct bmpCDT * bmpADT;
/*
 * Funciones
 */
/* Carga una estructura de BMP de un archivo *.bmp */
bmpADT loadBMP(char *filename);
/* Muestra datos del archivo bmp */
void showBMPInfo(bmpADT bmp);
/* Invierte los pixeles de la imagen */
void invertBMP(bmpADT bmp);
/* Mezcla los pixeles de la imagen*/
void scrambleBMP(bmpADT bmp);
/* Desmezcla los pixeles de la imagen*/
void descrambleBMP(bmpADT bmp);
/* Normaliza a un tamaño máximo de pixel */
void normalizeBMP(bmpADT bmp, BYTE maxVal);
/* Carga pixeles nuevos*/
void loadPixels(bmpADT bmp, BYTE *pixels, DWORD k);
/* Graba en el header de BMP la semilla y la posicion del shade (empezando en 1)*/
int alterHeaderBMP(bmpADT bmp, int shadeorder);
/* Guarda la estructura de BMP en un archivo de extensión *.bmp */
int saveBMP(bmpADT bmp, char *filename);
/* Cambia el nombre del archivo*/
void changeFileName(bmpADT bmp, char *newname);
/* Obtiene tamaño de la imagen*/
DWORD  getImageSize(bmpADT bmp);
/* Obtiene bloque de pixels completo*/
DWORD getPixelBlock(bmpADT bmp, BYTE **block);
/* Obtiene nombre del archivo*/
char *getFileName(bmpADT bmp);
/* Libera los recursos reservados por el ADT */
void freeBMP(bmpADT bmp);
#endif
