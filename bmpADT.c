#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "bmpADT.h"
#include "random.h"
#define BI_RGB  0
#define BI_RLE8 1
#define BI_RLE4 2
#define BI_BITFIELDS    3
#define BI_JPEG  4
#define BI_PNG  5

#define NORMALHEADERSIZE    54

typedef struct{
    BYTE    bfType[2];  /*must be BM*/
    DWORD   bfSize;     /*size of the bitmap file*/
    WORD    bfReserved1;
    WORD    bfReserved2;
    DWORD   bfOffBits;  /*offset in bytes from the beginning of the bitmap file header to the bitmap bits*/
}tbmpFileHeader;

typedef struct{
    DWORD   biSize;     /*Number of bytes required by the structure*/
    LONG    biWidth;    /*width of bitmap in pixels*/
    LONG    biHeight;   /*height of bitmap in pixels. Positive: bottom-up, Negative: top-down*/
    WORD    biPlanes;   /*Must be set to 1*/
    WORD    biBitCount; /*bits per pixel - Must be 8 bits in our case*/
    DWORD   biCompression;  /*must be BL_RGB in our case - uncompressed*/
    DWORD   biSizeImage;    /*size in bytes of the image Must be 0 for BI_RGB bitmaps*/
    LONG    biXPelsPerMeter;    /*Horizontal resolution*/
    LONG    biYPelsPerMeter;    /*Vertical resolution*/
    DWORD   biClrUsed;          /*Number of color indexes in the color table that are actually used*/
    DWORD   biClrImportant;     /*Number of color indexes that are required to display the bitmap (zero = all)*/
}tbmpInfoHeader;

typedef struct{
    BYTE rgbBlue;
    BYTE rgbGreen;
    BYTE rgbRed;
    BYTE rgbReserved;
}trgbQuad;

typedef struct{
    tbmpInfoHeader  bmiHeader;
    trgbQuad        bmiColors[1];/*mustbe NULL in our case*//*No la tengo en cuenta por ser B&N*/
}tbmpInfo;

typedef struct{
    int size;
    BYTE *extra;
}tbmpExtra;
struct bmpCDT
{
    char            *filename;
    tbmpFileHeader  fileHeader;
    tbmpInfo        info;
    tbmpExtra       extradata;
    BYTE            *pixels;/*color_index array*/
};
/*
 * Functions
 */

bmpADT
loadBMP(char *filename)
{
    bmpADT aux;
    tbmpFileHeader  fhdr;
    tbmpInfo        finfo;
    tbmpExtra       fextra;

    FILE *fImagen;

    if ((fImagen = fopen(filename,"rb"))==NULL)
        return NULL;
    if ((aux = malloc (sizeof(struct bmpCDT)))!=NULL)
    {
        if((aux->filename = malloc(strlen(filename)+1))==NULL)
            return NULL;
        strcpy(aux->filename, filename);
        /*Load file header*/
        fread(&(fhdr.bfType), sizeof(char), 2, fImagen);
        fread(&(fhdr.bfSize), sizeof(DWORD), 1, fImagen);

        fread(&(fhdr.bfReserved1), sizeof(WORD), 1, fImagen);

        fread(&(fhdr.bfReserved2), sizeof(WORD), 1, fImagen);

        fread(&(fhdr.bfOffBits), sizeof(DWORD), 1, fImagen);

        aux->fileHeader = fhdr;
        /*Load bmp Info*/
        fread(&(finfo.bmiHeader.biSize), sizeof (DWORD), 1, fImagen);
        fread(&(finfo.bmiHeader.biWidth), sizeof (LONG), 1, fImagen);
        fread(&(finfo.bmiHeader.biHeight), sizeof (LONG), 1, fImagen);
        fread(&(finfo.bmiHeader.biPlanes), sizeof (WORD), 1, fImagen);
        fread(&(finfo.bmiHeader.biBitCount), sizeof (WORD), 1, fImagen);
        fread(&(finfo.bmiHeader.biCompression), sizeof (DWORD), 1, fImagen);
        fread(&(finfo.bmiHeader.biSizeImage), sizeof (DWORD), 1, fImagen);
        fread(&(finfo.bmiHeader.biXPelsPerMeter), sizeof (LONG), 1, fImagen);
        fread(&(finfo.bmiHeader.biYPelsPerMeter), sizeof (LONG), 1, fImagen);
        fread(&(finfo.bmiHeader.biClrUsed), sizeof (DWORD), 1, fImagen);
        fread(&(finfo.bmiHeader.biClrImportant), sizeof (DWORD), 1, fImagen);
        /*Load pixels*/

        /*fread(&(finfo.bmiColors[0]), sizeof(trgbQuad),1, fImagen);*/
        aux->info = finfo;
        /*Load extra data*/
        fextra.size = fhdr.bfOffBits - NORMALHEADERSIZE;
        if (fextra.size == 0)
            fextra.extra = NULL;
        else
        {
            fextra.extra = malloc(fextra.size);
            if (fextra.extra != NULL)
            {
                 fread(fextra.extra, fextra.size, 1, fImagen);
            }
        }
        aux->extradata = fextra;
        /*Load pixels*/
        if ((aux->pixels = malloc(fhdr.bfSize - fhdr.bfOffBits))==NULL)
        {
            free(aux);
            return NULL;
        }
        fread(aux->pixels,(fhdr.bfSize - fhdr.bfOffBits), 1, fImagen);
    }
    fclose(fImagen);
	return aux;
}
void
showBMPInfo(bmpADT bmp)
{
    tbmpFileHeader  fhdr;
    tbmpInfo        finfo;
    fhdr = bmp->fileHeader;
    printf("\nDATOS DE LA IMAGEN %s\n", bmp->filename);
    printf("Tipo: %c%c\n", fhdr.bfType[0], fhdr.bfType[1]);
    printf("Tamaño Archivo: %lu bytes\n", fhdr.bfSize);
    printf("Offset: %lu\n", fhdr.bfOffBits);
    finfo = bmp->info;
    printf("Ancho: %lu pixels\n", finfo.bmiHeader.biWidth);
    printf("Alto: %lu pixels\n", finfo.bmiHeader.biHeight);
    printf("Numero de planos: %d\n", finfo.bmiHeader.biPlanes);
    printf("Bits por pixel: %d\n", finfo.bmiHeader.biBitCount);
    printf("Comprimido:%s\n", (finfo.bmiHeader.biCompression == BI_RGB)? "no": "si");
    switch(finfo.bmiHeader.biCompression)
    {
        case BI_RGB: printf("No comprimido\n");break;
        case BI_RLE8: printf("RLE 8\n");break;
        case BI_RLE4: printf("RLE 4\n");break;
        case BI_BITFIELDS: printf("Bitfield\n");break;
        case BI_JPEG: printf("JPG\n");break;
        case BI_PNG: printf("PNG\n");break;
    }
    printf("Extra data: %ld\n", bmp->extradata.size);
    printf("\n");
    printf("Tamaño imagen:%ld\n", fhdr.bfSize - fhdr.bfOffBits);
}
void
invertBMP(bmpADT bmp)
{
    DWORD i;
    for (i = 0; i < (bmp->fileHeader.bfSize - bmp->fileHeader.bfOffBits); i++)
    {
       bmp->pixels[i] = ~(bmp->pixels[i]);
    }
}
void
scrambleBMP(bmpADT bmp)
{
    DWORD i;
    DWORD j;
    BYTE pixel;
    DWORD n;
    unsigned long int *sec;
    randomizeSeed();
    n = (bmp->fileHeader.bfSize - bmp->fileHeader.bfOffBits);
    sec = generaSec((unsigned long int) n);
    for (i = n - 1; i > 0; i--)
    {
        pixel = bmp->pixels[i];
        j = sec[n-i-1];
        bmp->pixels[i] = bmp->pixels[j];
        bmp->pixels[j] = pixel;
    }
    free(sec);
}
void
descrambleBMP(bmpADT bmp)
{
    DWORD i;
    DWORD j;
    BYTE pixel;
    DWORD n;
    long int *sec;
    randomizeSeed();
    n = (bmp->fileHeader.bfSize - bmp->fileHeader.bfOffBits);
    sec = generaSec(n);
    for (i = 1; i < n; i++)
    {
        pixel = bmp->pixels[i];
        j = sec[n-i-1];
        bmp->pixels[i] = bmp->pixels[j];
        bmp->pixels[j] = pixel;
    }
    free(sec);
}
void
normalizeBMP(bmpADT bmp, BYTE maxVal)
{
    int i;
    for (i = 0; i < (bmp->fileHeader.bfSize - bmp->fileHeader.bfOffBits); i++)
    {
       if (bmp->pixels[i]>maxVal)
        bmp->pixels[i] = maxVal;
    }
}


int alterHeaderBMP(bmpADT bmp, int shadeorder)
{
    tbmpFileHeader *fhdr;

    fhdr = &(bmp->fileHeader);
    /*Save file header*/


    fhdr->bfReserved1 = (int)SEED;
    fhdr->bfReserved2 = shadeorder;   
}


int
saveBMP(bmpADT bmp, char *filename)
{
    FILE *fImagen;
    tbmpFileHeader fhdr;
    tbmpInfo       finfo;
    tbmpExtra      fextra;

    if ((fImagen = fopen(filename,"wb"))==NULL)
        return FAILURE;
    fhdr = bmp->fileHeader;
    /*Save file header*/
    fwrite(&(fhdr.bfType), sizeof(char), 2, fImagen);
    fwrite(&(fhdr.bfSize), sizeof(DWORD), 1, fImagen);
    fwrite(&(fhdr.bfReserved1), sizeof(WORD), 1, fImagen);
    fwrite(&(fhdr.bfReserved2), sizeof(WORD), 1, fImagen);
    fwrite(&(fhdr.bfOffBits), sizeof(DWORD), 1, fImagen);

    finfo = bmp->info;
    /*Save bmp Info*/
    fwrite(&(finfo.bmiHeader.biSize), sizeof (DWORD), 1, fImagen);
    fwrite(&(finfo.bmiHeader.biWidth), sizeof (LONG), 1, fImagen);
    fwrite(&(finfo.bmiHeader.biHeight), sizeof (LONG), 1, fImagen);
    fwrite(&(finfo.bmiHeader.biPlanes), sizeof (WORD), 1, fImagen);
    fwrite(&(finfo.bmiHeader.biBitCount), sizeof (WORD), 1, fImagen);
    fwrite(&(finfo.bmiHeader.biCompression), sizeof (DWORD), 1, fImagen);
    fwrite(&(finfo.bmiHeader.biSizeImage), sizeof (DWORD), 1, fImagen);
    fwrite(&(finfo.bmiHeader.biXPelsPerMeter), sizeof (LONG), 1, fImagen);
    fwrite(&(finfo.bmiHeader.biYPelsPerMeter), sizeof (LONG), 1, fImagen);
    fwrite(&(finfo.bmiHeader.biClrUsed), sizeof (DWORD), 1, fImagen);
    fwrite(&(finfo.bmiHeader.biClrImportant), sizeof (DWORD), 1, fImagen);
    /*Omit this
    fwrite(&(finfo.bmiColors[0]), sizeof(trgbQuad),1, fImagen);*/
    /*Save extra data*/
    fextra = bmp->extradata;
    if (fextra.size != 0)
        fwrite(fextra.extra, fextra.size, 1, fImagen);

    fwrite(bmp->pixels, fhdr.bfSize - fhdr.bfOffBits, 1, fImagen);

    fclose(fImagen);
    return OK;
}
DWORD
getImageSize(bmpADT bmp)
{
        return (bmp->fileHeader.bfSize - bmp->fileHeader.bfOffBits);
}
char *
getFileName(bmpADT bmp)
{
    return (bmp->filename);
}
DWORD
getPixelBlock(bmpADT bmp, BYTE **block)
{
    DWORD imsize;
    imsize = bmp->fileHeader.bfSize - bmp->fileHeader.bfOffBits;
    *block = malloc(imsize);
    if (*block == NULL)
        return 0;
    memcpy(*block, bmp->pixels, imsize);
    return imsize;
}
void
changeFileName(bmpADT bmp, char *newname)
{
        bmp->filename = realloc(bmp->filename, strlen(newname) + 1);
        strcpy(bmp->filename, newname);
}

void
freeBMP(bmpADT bmp)
{
    free(bmp->pixels);
    free(bmp);
}

void
loadPixels(bmpADT bmp, BYTE *pixels, DWORD k)
{
    memcpy(bmp->pixels, pixels, k);
}
