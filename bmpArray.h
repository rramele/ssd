#ifndef BMPARRAY_H_INCLUDED
#define BMPARRAY_H_INCLUDED
#include "handleDir.h"
#include "bmpADT.h"

typedef struct{
    bmpADT  *pics;
    int size;
}tbmpArray;

/* Obtiene cantidad de bmps de un determinado tamaño*/
int obtainBmpsSize(tNameArr *picList, DWORD imageSize);
/* Obtiene lista de imagenes de un determinado tamaño en bmpArr*/
int obtainBmpsSizeArray(tNameArr *picList, DWORD imageSize, tbmpArray *bmpArr);
/* Libera el arreglo de imagenes*/
void freeBmpsArray(tbmpArray *bmpArr);
/* Invierte colores en todas las imagenes del arreglo*/
void   invertAllBmpArray(tbmpArray *bmpArray);
/* Guarda en archivos bmp cada imagen*/
int saveAllBmpArray(tbmpArray *bmpArray);
#endif // BMPARRAY_H_INCLUDED
