/*
** 	Interface para la biblioteca random
**	Version 1.0
**	Autores G & G
**	Fecha de ultima modificacion 01/01/1998
*/

#ifndef  _RANDOM_H_
#define	 _RANDOM_H_
#define SEED    691
/*	Esta funcion devuelve un n�mero double pseudo-aleatorio perteneciente
**  	al intervalo [0, 1)
**	Ejemplo de uso: double n= randNormalize();
*/
double randNormalize(void);


/* 	Esta funcion devuelve un n�mero entero pseudo-aleatorio perteneciente
**	al intervalo entero [izq, der]
**	Ejemplo de uso: int n= randInt(izq, der);
*/
int randInt( int izq, int der);
/*
 * Genera una secuencia de numeros aleatorios
 */
unsigned long int *generaSec(unsigned long int n);

long int randInt2 (long int maxn);
/* 	Esta funcion devuelve un n�mero double pseudo-aleatorio perteneciente
**	al intervalo double [izq, der]
**	Ejemplo de uso: double n= randReal(izq, der);
*/
double randReal( double izq, double der);


/* 	Esta funci�n cambia la secuencia de n�meros pseudo-aleatorio a generar
**	en las  pr�ximas invocaciones de randNormalize, randInt y randReal.
**	Se la suele invocar una sola vez en la aplicaci�n, pero no hay problema
**	en usarla m�s veces.
**	Ejemplo de uso: randomize();
*/
void randomize(void);

void randomizeSeed(void);
#endif

