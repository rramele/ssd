#ifndef TYPES_H_INCLUDED
#define TYPES_H_INCLUDED
#include <stdint.h>
#define FAILURE 0
#define OK  !FAILURE
typedef uint8_t BYTE;
typedef uint16_t WORD;
typedef uint32_t DWORD;
typedef long LONG;
#endif // TYPES_H_INCLUDED
