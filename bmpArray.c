#include "bmpArray.h"
#include <stdlib.h>
#include <string.h>
static char *obtainNewName(char *oldnamewithext, char *add);
int
obtainBmpsSize(tNameArr *picList, DWORD imageSize)
{
    bmpADT bmp;
    int count;
    int i;
    for (i = 0, count = 0; i < picList->size; i++)
    {
        if ((bmp = loadBMP(picList->arr[i]))!=NULL)
        {
            if (getImageSize(bmp)==imageSize)
                count++;
            freeBMP(bmp);
        }
    }
    return count;
}

int
obtainBmpsSizeArray(tNameArr *picList, DWORD imageSize, tbmpArray *bmpArr)
{
    int i;
    int count;
    bmpArr->pics = malloc(sizeof (bmpADT)*bmpArr->size);
    for (i = 0, count = 0; i < picList->size && count < bmpArr->size; i++)
    {
        if ((bmpArr->pics[count] = loadBMP(picList->arr[i]))!=NULL)
        {
            if (getImageSize(bmpArr->pics[count])==imageSize)
                count++;
        }
    }
    return count;
}
void
invertAllBmpArray(tbmpArray *bmpArray)
{
    int i;
    char *oldname;
    char *newname;
    for (i = 0; i < bmpArray->size; i++)
    {
        invertBMP(bmpArray->pics[i]);
        oldname = getFileName(bmpArray->pics[i]);
        newname = obtainNewName(oldname, "inv");
        if(newname !=NULL)
        {
            changeFileName(bmpArray->pics[i], newname);
        }
    }
}
char *obtainNewName(char *oldnamewithext,char *add)
{
    char *newname;
    int len;
    int i;
    char *ext;
    i = 0;
    while(oldnamewithext[i]!='\0')
    {
        if (oldnamewithext[i]=='.')
            len = i;
        i++;
    }
    if((ext = malloc(strlen(oldnamewithext)-len))==NULL)
        return NULL;
    strcpy(ext, oldnamewithext + len);
    if((newname = malloc(strlen(oldnamewithext) + strlen(add)+1))==NULL)
        return NULL;
    strcpy(newname, oldnamewithext);
    newname[len] = '\0';
    strcat(newname, add);
    strcat(newname, ext);
    return newname;
}
int
saveAllBmpArray(tbmpArray *bmpArray)
{
    int i;
    char *filename;

    for (i = 0; i < bmpArray->size; i++)
    {
        filename = getFileName(bmpArray->pics[i]);
        alterHeaderBMP(bmpArray->pics[i], i+1);
        saveBMP(bmpArray->pics[i], filename);
    }
    return OK;
}
void
freeBmpsArray(tbmpArray *bmpArr)
{
    int i;
    for (i = 0; i < bmpArr->size; i++)
        freeBMP(bmpArr->pics[i]);
    free(bmpArr->pics);
}
