#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include "handleDir.h"
#define BLOCK   2
static int hasExtension(struct dirent *entry, char *ext);

int listdir(char *path)
{
    struct dirent *entry;
    DIR *dp;
    /*
        Designar un puntero al directorio.
        Si no se puede, es que no existe
    */
    dp = opendir(path);
    if (dp == NULL)
    {
        return FAILURE;
    }
    /*Recorrer*/
    while ((entry = readdir(dp))!=NULL)
        printf("%s\n",entry->d_name);
    closedir(dp);
    return OK;
}
tNameArr *filenamesdir(char *path)
{
   return filenamesdirext(path, NULL);
}
tNameArr *filenamesdirext(char *path, char *ext)
{
    struct dirent *entry;
    DIR *dp;
    tNameArr *fileList;
    char **aux;

    dp = opendir(path);
    if (dp == NULL)
        return NULL;

    if ((fileList = malloc(sizeof (tNameArr)))==NULL)
        return NULL;

    fileList->size = 0;
    fileList->arr = NULL;
    /*Recorrer*/
    while ((entry = readdir(dp))!=NULL)
    {
        if (ext ==  NULL || hasExtension(entry, ext))
        {
            if(fileList->size % BLOCK == 0)
            {
                if ((aux  = realloc(fileList->arr, (fileList->size + BLOCK) * sizeof (char*)))==NULL)
                    return NULL;
                fileList->arr = aux;
            }
            fileList->arr[fileList->size]= malloc(strlen(entry->d_name) + strlen(path) + 1 +1);
            strcpy(fileList->arr[fileList->size], path);
            strcat(fileList->arr[fileList->size], "/");
            strcat(fileList->arr[fileList->size], entry->d_name);
            (fileList->size)++;
        }
    }
    closedir(dp);
    if (fileList->size == 0)
    {
        free(fileList);
        fileList = NULL;
    }
    return fileList;
}
int showfilenames(tNameArr *fileList)
{
    int i;
    for (i = 0; i < fileList->size; i++)
    {
        printf("%s\n",fileList->arr[i]);
    }
    printf("%d archivos\n", fileList->size);
    return (fileList->size);
}
static int hasExtension(struct dirent *entry, char *ext)
{
    int i = 0, lastpoint=0;
    while(entry->d_name[i]!='\0')
    {
        if(entry->d_name[i]=='.')
            lastpoint = i;
        i++;
    }
    return(!strcmp(entry->d_name + lastpoint + 1, ext));
}
